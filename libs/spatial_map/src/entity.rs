use uuid::Uuid;

#[derive(Copy, Clone)]
pub struct Entity {
    pub id: Uuid,
    pub x: f64, 
    pub y: f64
}

impl Entity {
    pub fn new(x: f64, y: f64) -> Entity {
        Entity {
            id: Uuid::new_v4(),
            x,
            y
        }
    }
}

